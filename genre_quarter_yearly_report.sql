/*
	B.  Provide original code for function(s) in text format that perform the transformation(s) you identified in part A4.
At first I overcomplicated this in my head and tried to create a case statement for each month. It wasn't working well and then I remembered that there are conversion functions for time.
*/

CREATE OR REPLACE FUNCTION extract_quarter(date_input TIMESTAMP WITHOUT TIME ZONE)
RETURNS INT
AS $$
BEGIN
  RETURN CASE
	WHEN EXTRACT(MONTH FROM date_input) IN (1, 2, 3) THEN 1
	WHEN EXTRACT(MONTH FROM date_input) IN (4, 5, 6) THEN 2
	WHEN EXTRACT(MONTH FROM date_input) IN (7, 8, 9) THEN 3
	WHEN EXTRACT(MONTH FROM date_input) IN (10, 11, 12) THEN 4
  END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION extract_year(date_input TIMESTAMP WITHOUT TIME ZONE)
RETURNS INT AS $$
BEGIN
  RETURN EXTRACT(YEAR FROM date_input);
END;
$$ LANGUAGE plpgsql;

/*
	C.  Provide original SQL code in a text format that creates the detailed and summary tables to hold your report table sections.
*/
DROP TABLE IF EXISTS genre_quarter_summary;
CREATE TABLE genre_quarter_summary(
  quarter_rented INT NOT NULL,
  year_rented INT NOT NULL,
  genre_id INT NOT NULL REFERENCES category(category_id),
  rental_count INT NOT NULL,
  total_sales DECIMAL(10, 2) NOT NULL
);

DROP TABLE IF EXISTS genre_quarter_detail;
CREATE TABLE genre_quarter_detail (
rental_id INT REFERENCES rental(rental_id),
genre_id INT REFERENCES category(category_id),
film_id INT REFERENCES film(film_id),
rental_rate NUMERIC(4,2),
quarter_rented INT NOT NULL,
year_rented INT NOT NULL,
rental_date TIMESTAMP WITHOUT time zone
);

/*
D. Provide an original SQL query in a text format that will extract the raw data needed for the detailed section of your report from the source database.  
*/

INSERT INTO genre_quarter_detail
SELECT r.rental_id, c.category_id AS genre_id,
	f.film_id, p.amount as rental_rate,
	extract_quarter(r.rental_date) AS ex_quarter,
	extract_year(r.rental_date) AS ex_year,
	r.rental_date
FROM rental r
INNER JOIN inventory inv ON r.inventory_id = inv.inventory_id
INNER JOIN film f ON inv.film_id = f.film_id
INNER JOIN film_category fc ON f.film_id = fc.film_id
INNER JOIN category c ON fc.category_id = c.category_id
INNER JOIN payment p ON p.rental_id = r.rental_id
WHERE r.rental_date >= '2005-01-01'
	AND r.rental_date <= '2008-01-01'
GROUP BY r.rental_id, genre_id, ex_quarter, ex_year,
	f.film_id, p.amount
ORDER BY r.rental_date asc;

/*
E.  Provide original SQL code in a text format that creates a trigger on the detailed table of the
report that will continually update the summary table as data is added to the detailed table.
*/

DROP FUNCTION IF EXISTS  update_genre_quarter_summary CASCADE;
CREATE FUNCTION update_genre_quarter_summary()
RETURNS trigger AS $$
BEGIN
INSERT INTO genre_quarter_summary (quarter_rented, year_rented, genre_id, rental_count, total_sales)
SELECT gqd.quarter_rented, gqd.year_rented, gqd.genre_id, COUNT(*), SUM(p.amount)
FROM genre_quarter_detail gqd
INNER JOIN rental r ON gqd.rental_id = r.rental_id
INNER JOIN payment p ON r.rental_id = p.rental_id
GROUP BY gqd.quarter_rented, gqd.year_rented, gqd.genre_id;
RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_summary_trigger
AFTER INSERT ON genre_quarter_detail
FOR EACH ROW EXECUTE PROCEDURE update_genre_quarter_summary();

/*
F.  Provide an original stored procedure in a text format that can be used to refresh the data in both the detailed table and summary table. The procedure should clear the contents of the detailed table and summary table and perform the raw data extraction from part D.
*/
DROP PROCEDURE IF EXISTS refresh_genre_data CASCADE;
CREATE OR REPLACE PROCEDURE refresh_genre_data()
AS $$
BEGIN
-- Clear existing genre_quarter_detail data
TRUNCATE genre_quarter_detail, genre_quarter_summary;

-- Insert new data from rental and payment tables
INSERT INTO genre_quarter_detail
SELECT r.rental_id, c.category_id AS genre_id,
	f.film_id, p.amount as rental_rate,
	extract_quarter(r.rental_date) AS quarter_rented,
	extract_year(r.rental_date) AS year_rented,
	r.rental_date
FROM rental r
INNER JOIN inventory inv ON r.inventory_id = inv.inventory_id
INNER JOIN film f ON inv.film_id = f.film_id
INNER JOIN film_category fc ON f.film_id = fc.film_id
INNER JOIN category c ON fc.category_id = c.category_id
INNER JOIN payment p ON p.rental_id = r.rental_id
WHERE r.rental_date >= '2005-01-01'
AND r.rental_date <= '2008-01-01'
GROUP BY r.rental_id, genre_id, quarter_rented, year_rented,
	f.film_id, p.amount
ORDER BY r.rental_date asc;
RETURN;
END;
$$ LANGUAGE plpgsql;

CALL refresh_genre_data();

SELECT * FROM genre_quarter_detail;
SELECT * FROM genre_quarter_summary 
GROUP BY quarter_rented, year_rented, genre_id, rental_count, total_sales
ORDER BY quarter_rented, genre_id
limit 100;
