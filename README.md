 
Paul Cameron
D326 - Advanced Data Management
02/08/2024

https://wgu.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=106d88cf-cd53-43e5-9d82-b11c00c80b8f

<h1>DVD Database</h1>
<h2>Introduction</h2>

The purpose of this paper is to design and implement a database for a DVD collection. The database will store information about DVDs, such as the title, director, actors, and release date. It will also allow users to track their collections and create custom reports.

<h2>Written Report</h2>

A. Summarize one real-world written business report that can be created from the DVD Dataset from the “Labs on Demand Assessment Environment and DVD Database” attachment.

    Quarterly Genre-Based Sales Reports: A Strategic Tool for the Film Industry

    For film studios and distributors, understanding the performance of different movie genres across various quarters proves invaluable. Quarterly reports highlighting total sales per genre offer a wealth of information crucial for making informed decisions that impact the entire industry.

    Navigating Trends and Optimizing Resources: These reports unveil trends and patterns in audience preferences, allowing studios to identify genres on the rise or in decline. This knowledge becomes the cornerstone for strategic adjustments in production, marketing, and acquisition. By strategically allocating resources towards genres with higher potential returns, studios can maximize their financial success.

    Evaluating Performance and Identifying Opportunities: Comparing the sales performance of different genres within a quarter allows for a clear evaluation of each category's relative success. This enables studios to assess the effectiveness of their existing efforts and pinpoint areas where improvement is needed. Additionally, benchmarking performance against competitors within the same genre provides valuable insights into industry trends and competitor strategies, further informing strategic decisions.

1. Identify the specific fields that will be included in the detailed table and the summary table of the report.

        The Detail table will have the following. rental_id, genre_id, film_id, rental_rate, quarter_rented, year_rented, and rental_date 

        The Summary table will consists of the following: quarter_rented, year_rented, genre_id, rental_count, and total_sales

2. Describe the types of data fields used for the report.
            
        Genre_Quarter_Summary
        - Quarter_rented: Quarter for the genre summary (INT)
        - Year_rented: Year of the genre summary (INT)
        - Genre_id: References ids in the category table (INT)
        - Rental_count: A total count of rentals per genre for the quarter (INT) 
        - Total_sales: Total rental sales of the genre for that quarter (DECIMAL)

		Genre_Quarter_Detail
        - Rental_id: References ids in the rental table (INT)
        - Genre_id: References ids in the category table (INT)
        - Film_id: References ids in the film table (INT)
        - Rental_rate: Rental rate per video (NUMERIC)
        - Quarter_rented: Quarter when the individual video was rented (INT)
        - Year_rented: Year when the individual video was rented (INT)
        - Rental_date: Rental date of the video (TIMESTAMP)  

3. Identify at least two specific tables from the given dataset that will provide the data necessary for the detailed table section and the summary table section of the report.
		
        The detail table data comes from film, film_category, payment, and category while the summary table will pull data from rental and payment

4. Identify at least one field in the detailed table section that will require a custom transformation with a user-defined function and explain why it should be transformed (e.g., you might translate a field with a value of N to No and Y to Yes).
		
        The payment_date field in table payment can be transformed to be based on quarters instead of timestamp dates. Aligning with #6, simplifying the readability of quarterly reports

5. Explain the different business uses of the detailed table section and the summary table section of the report.

        Genre Quarter Detail will be our detail table and that gives us individual information about each movie rented, their film details, rental rate, and checkout date.
        Some of the business use cases: 
        - Identify any outliers in rental rates
        - Recommend similar movies to users who rented a particular film

        Our summary table tells us about film genre and total sales based on yearly quarters. Some of the business use cases: 
        - Identify the most profitable quarter based on rental rates
        - Compare film rental trends across different quarters

6. Explain how frequently your report should be refreshed to remain relevant to stakeholders.

        This report should be run once a quarter to understand movie trends and maximize the marketing of certain genres. Each movie has a small window to stay relevant before the next movie piques the interest of the customer. I believe that window is every 12-14 weeks 

B. Provide original code for function(s) in text format that performs the transformation(s) you identified in part A4.

    /* 
    Receiving an error about not containing a timezone. I had to explicitly state no timezone was reported in the source of the record 
    */
    CREATE OR REPLACE FUNCTION payment_quarter(date_input TIMESTAMP WITHOUT TIME ZONE)
    RETURNS INTEGER
    AS $$
    BEGIN
    RETURN CASE
        WHEN EXTRACT(MONTH FROM date_input) IN (1, 2, 3) THEN 1
        WHEN EXTRACT(MONTH FROM date_input) IN (4, 5, 6) THEN 2
        WHEN EXTRACT(MONTH FROM date_input) IN (7, 8, 9) THEN 3
        WHEN EXTRACT(MONTH FROM date_input) IN (10, 11, 12) THEN 4
    END;
    END;
    $$ LANGUAGE plpgsql;

        CREATE OR REPLACE FUNCTION extract_year(date_input TIMESTAMP WITHOUT TIME ZONE)
        RETURNS INT AS $$
        BEGIN
        RETURN EXTRACT(YEAR FROM date_input);
        END;
        $$ LANGUAGE plpgsql;

    C.  Provide original SQL code in a text format that creates the detailed and summary tables to hold your report table sections.

    /* 
    Identify the top and least watched genres for each quarter, based on rental count or total
    sales. 
    */

    CREATE TABLE genre_quarter_summary(
    quarter_rented INT NOT NULL,
    year_rented INT NOT NULL,
    genre_id INT NOT NULL REFERENCES category(category_id),
    rental_count INT NOT NULL,
    total_sales DECIMAL(10, 2) NOT NULL
    );

    /* 
    Join the genre_quarter_detail table with the genre_quarter_summary table for specific genres and quarters to provide detailed information about individual rentals 
    */

    CREATE TABLE genre_quarter_detail (
    rental_id INT REFERENCES rental(rental_id),
    genre_id INT REFERENCES category(category_id),
    film_id INT REFERENCES film(film_id),
    rental_rate NUMERIC(4,2),
    quarter_rented INT NOT NULL,
    year_rented INT NOT NULL,
    rental_date TIMESTAMP WITHOUT time zone
    );

D. Provide an original SQL query in a text format that will extract the raw data needed for the detailed section of your report from the source database.  

    INSERT INTO genre_quarter_detail
    SELECT r.rental_id, c.category_id AS genre_id,
        f.film_id, p.amount as rental_rate,
        extract_quarter(r.rental_date) AS ex_quarter,
        extract_year(r.rental_date) AS ex_year,
        r.rental_date
    FROM rental r
    INNER JOIN inventory inv ON r.inventory_id = inv.inventory_id
    INNER JOIN film f ON inv.film_id = f.film_id
    INNER JOIN film_category fc ON f.film_id = fc.film_id
    INNER JOIN category c ON fc.category_id = c.category_id
    INNER JOIN payment p ON p.rental_id = r.rental_id
    WHERE r.rental_date >= '2005-01-01'
        AND r.rental_date <= '2008-01-01'
    GROUP BY r.rental_id, genre_id, ex_quarter, ex_year,
        f.film_id, p.amount
    ORDER BY r.rental_date asc;

E.  Provide original SQL code in a text format that creates a trigger on the detailed table of the
report that will continually update the summary table as data is added to the detailed table.

    CREATE FUNCTION update_genre_quarter_summary()
    RETURNS trigger AS $$
    BEGIN
    INSERT INTO genre_quarter_summary (quarter_rented, year_rented, genre_id, rental_count, total_sales)
    SELECT gqd.quarter_rented, gqd.year_rented, gqd.genre_id, COUNT(*), SUM(p.amount)
    FROM genre_quarter_detail gqd
    INNER JOIN rental r ON gqd.rental_id = r.rental_id
    INNER JOIN payment p ON r.rental_id = p.rental_id
    GROUP BY gqd.quarter_rented, gqd.year_rented, gqd.genre_id;
    RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;

    CREATE TRIGGER update_summary_trigger
    AFTER INSERT ON genre_quarter_detail
    FOR EACH ROW EXECUTE PROCEDURE update_genre_quarter_summary();

F.  Provide an original stored procedure in a text format that can be used to refresh the data in both the detailed table and summary table. The procedure should clear the contents of the detailed table and summary table and perform the raw data extraction from part D.

    CREATE OR REPLACE PROCEDURE refresh_genre_data()
    AS $$
    BEGIN
    -- Clear existing genre_quarter_detail and genre_quarter_summary data
    TRUNCATE genre_quarter_detail, genre_quarter_summary;

    -- Insert new data from rental and payment tables
    INSERT INTO genre_quarter_detail
    SELECT r.rental_id, c.category_id AS genre_id,
        f.film_id, p.amount as rental_rate,
        extract_quarter(r.rental_date) AS quarter_rented,
        extract_year(r.rental_date) AS year_rented,
        r.rental_date
    FROM rental r
    INNER JOIN inventory inv ON r.inventory_id = inv.inventory_id
    INNER JOIN film f ON inv.film_id = f.film_id
    INNER JOIN film_category fc ON f.film_id = fc.film_id
    INNER JOIN category c ON fc.category_id = c.category_id
    INNER JOIN payment p ON p.rental_id = r.rental_id
    WHERE r.rental_date >= '2005-01-01'
    AND r.rental_date <= '2008-01-01'
    GROUP BY r.rental_id, genre_id, quarter_rented, year_rented,
        f.film_id, p.amount
    ORDER BY r.rental_date asc;
    RETURN;
    END;
    $$ LANGUAGE plpgsql;

    CALL refresh_genre_data();

    SELECT * FROM genre_quarter_detail;
    SELECT * FROM genre_quarter_summary
    GROUP BY quarter_rented, year_rented, genre_id, rental_count, total_sales
    ORDER BY quarter_rented, genre_id
    limit 100;

1.  Identify a relevant job scheduling tool that can be used to automate the stored procedure.

        PostgresSQL has a tool called, “pgAgent Job” that manages jobs abd schedules. The soonest I would run these jobs starting the next following day after the end of each quarter. For example, April 1st, July 1st, Oct. 1st, and Jan. 1st since the report can be evaluated for the best performing genre on the previous quarter. 

G.  Provide a Panopto video recording that includes the presenter and a vocalized demonstration of the functionality of the code used for the analysis.

H.  Acknowledge all utilized sources, including any sources of third-party code, using in-text citations and references. If no sources are used, clearly declare that no sources were used to support your submission.

	No sources were used for this submission.

I.  Demonstrate professional communication in the content and presentation of your submission.
