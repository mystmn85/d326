# Placement for all the sql commands

<pre>
select f.title, f.rental_rate, c.name as genre
from film_category as fc
right join film as f
on f.film_id = fc.film_id
inner join category as c
on fc.category_id = c.category_id;
</pre>

select f.title, f.rental_rate, c.name as genre
from film_category as fc
right join film as f
on f.film_id = fc.film_id
inner join category as c
on fc.category_id = c.category_id

select * from category;
select * from film_category;

select * from film;
select * from payment
order by rental_id desc;

<pre>X
WITH film_with_rental_count AS (
  SELECT film.*, COUNT(rental.*) AS rental_count
  FROM film
  JOIN inventory USING (film_id)
  JOIN rental USING (inventory_id)
  GROUP BY film.film_id
)
SELECT
  category.name,
  film_with_rental_count.title,
  film_with_rental_count.rental_count
FROM film_with_rental_count
JOIN film_category USING (film_id)
JOIN category USING (category_id)
ORDER BY category.name, rental_count DESC;
</pre>

<pre>

CREATE OR REPLACE FUNCTION payment_quarter(date_input TIMESTAMP WITHOUT TIME ZONE)
RETURNS INTEGER
AS $$
BEGIN
  RETURN CASE
    WHEN EXTRACT(MONTH FROM date_input) IN (1, 2, 3) THEN 1
    WHEN EXTRACT(MONTH FROM date_input) IN (4, 5, 6) THEN 2
    WHEN EXTRACT(MONTH FROM date_input) IN (7, 8, 9) THEN 3
    WHEN EXTRACT(MONTH FROM date_input) IN (10, 11, 12) THEN 4
  END;
END;
$$ LANGUAGE plpgsql;

SELECT
    payment_id,
    customer_id,
    amount,
    payment_date,
    payment_quarter(payment_date) AS quarter
FROM payment 
WHERE payment_quarter(payment_date) = 2;
</pre>

## Extracting raw data for detailed section of genre_quarter_detail
<pre>
...
WHERE r.rental_date >= '2005-01-01'
  AND r.rental_date <= '2006-01-01'
  AND c.category_id = 1;
</pre>
<pre>
CREATE TABLE genre_quarter_detail (
rental_id INT REFERENCES rental(rental_id),
genre_id INT REFERENCES category(category_id),
film_id INT REFERENCES film(film_id),
rental_rate NUMERIC(4,2),
quarter INT,
rental_date TIMESTAMP without time zone
);

CREATE TABLE genre_quarter_summary(
  genre_quarter_id INTEGER,
  quarter INTEGER NOT NULL,
  genre_id INTEGER NOT NULL REFERENCES category(category_id),
  rental_count INTEGER NOT NULL,
  total_sales DECIMAL(10, 2) NOT NULL
);
</pre>
### DELETE FROM genre_quarter_detail;
<pre>
INSERT INTO genre_quarter_detail
SELECT r.rental_id, c.category_id AS genre_id,
   f.film_id, p.amount as rental_rate, 
   payment_quarter(r.rental_date) AS quarter,
   r.rental_date
FROM rental r
INNER JOIN inventory inv ON r.inventory_id = inv.inventory_id
INNER JOIN film f ON inv.film_id = f.film_id
INNER JOIN film_category fc ON f.film_id = fc.film_id
INNER JOIN category c ON fc.category_id = c.category_id
INNER JOIN payment p ON p.rental_id = r.rental_id
WHERE r.rental_date >= '2003-01-01'
  AND r.rental_date <= '2006-01-01'
  GROUP BY r.rental_id, genre_id, quarter,
       f.film_id, p.amount
  ORDER BY r.rental_date asc;
</pre>

<pre>
CREATE FUNCTION update_genre_quarter_summary()
RETURNS trigger AS $$
BEGIN
  -- Update genre_quarter_summary table
  UPDATE genre_quarter_summary
  SET 
    rental_count = sqd.rental_count,
    total_sales = sqd.total_amount
  FROM (
    SELECT
      genre_id,
      quarter,
      SUM(p.amount) AS total_amount,
      COUNT(*) AS rental_count
    FROM genre_quarter_detail
    INNER JOIN rental r ON genre_quarter_detail.rental_id = r.rental_id
    INNER JOIN payment p ON r.rental_id = p.rental_id
    GROUP BY genre_id, quarter
  ) AS sqd
  WHERE genre_id = sqd.genre_id AND quarter = sqd.quarter;

  RETURN NULL;
END;
$$ LANGUAGE plpgsql;
</pre>
<pre>
CREATE TRIGGER update_summary_trigger
AFTER INSERT ON genre_quarter_detail
FOR EACH ROW EXECUTE PROCEDURE update_genre_quarter_summary();
</pre>

### DROP FUNCTION update_genre_quarter_summary CASCADE;


### 2/17

SELECT
  genre_id,
  MAX(rental_count) AS highest_count
FROM (
  SELECT
    genre_id,
    COUNT(*) AS rental_count
  FROM genre_quarter_detail
  GROUP BY genre_id
) AS genre_counts
GROUP BY genre_id
ORDER BY highest_count desc

select * from genre_quarter_detail

SELECT SUM(p.amount)
	  FROM rental r
	  INNER JOIN payment p 
			ON r.rental_id = p.rental_id
	  WHERE r.rental_id = p.rental_id
	  
Select * from rental r
 INNER JOIN payment p 
			ON r.rental_id = p.rental_id